﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int vidas = 3;

    public List<Sprite> sprites;

    public SpriteRenderer danyos;

    public GameObject gameOverPanel;
    public AudioSource gameOverAudio;

    public static bool final;

    private void Start() {
        final = false;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Enemy")) {
            vidas--;
            danyos.sprite = sprites[vidas];
            if (vidas == 0) {
                final = true;
                gameOverPanel.SetActive(true);
                gameOverAudio.Play();
                PlayerMovement.pause = true;
                Time.timeScale = 0;
            }
        }
    }
}
