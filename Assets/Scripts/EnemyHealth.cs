﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour {

    public static int points;

    private void OnCollisionEnter2D(Collision2D collision) {
        // El enemigo se destruye en cuanto colisiona con algún objeto
        points += 10;
        GameObject.Find("ScoreText").GetComponent<Text>().text = points.ToString();
        Destroy(gameObject);
    }
}