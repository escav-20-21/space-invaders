﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpriteSelector : MonoBehaviour {
    [Tooltip("Referencia al SpriteRenderer del enemigo")]
    public SpriteRenderer spriteRenderer;

    [Tooltip("Listado de posibles sprites para el enemigo")]
    public List<Sprite> sprites;
    [Tooltip("Referencia al prefab de la bala")]
    public GameObject bulletPrefab;

    

    void Start() {
        // Elección aleatoria del sprite del enemigo
        spriteRenderer.sprite = sprites[Random.Range(0, sprites.Count)];
        InvokeRepeating("EnemyShot", Random.Range(2.0f, 12.0f), Random.Range(6.0f, 15.0f));
    }

    public void EnemyShot() {
        Instantiate(bulletPrefab, transform.position, Quaternion.identity);
    }
}