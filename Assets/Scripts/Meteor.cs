﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : MonoBehaviour {
    [Tooltip("Referencia al SpriteRenderer del enemigo")]
    public SpriteRenderer spriteRenderer;

    [Tooltip("Listado de posibles sprites para el enemigo")]
    public List<Sprite> sprites;

    void Start() {
        // Elección aleatoria del sprite del enemigo
        spriteRenderer.sprite = sprites[Random.Range(0, sprites.Count)];
        transform.position = new Vector3(Random.Range(-5.0f, 5.0f), Random.Range(0.0f, -3.0f), 0);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        Destroy(collision.gameObject);
    }
}
