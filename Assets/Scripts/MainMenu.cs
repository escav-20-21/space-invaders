﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Text uno;
    public Text dos;

    // Start is called before the first frame update
    void Start()
    {
        uno.text = "1.   " + PlayerPrefs.GetInt("best1", 0).ToString("0000");
        dos.text = "2.   " + PlayerPrefs.GetInt("best2", 0).ToString("0000");
    }

    public void Azul() {
        SceneManager.LoadScene("Game");

    }

    public void Verde() {
        SceneManager.LoadScene("Game_verde");

    }
}
