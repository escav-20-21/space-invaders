﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    [Tooltip("Velocidad del jugador")]
    public float playerSpeed;

    [Tooltip("Límite de la pantalla")]
    public float screenLimit;

    public GameObject pausePanel;

    public static bool pause;

    private void Start() {
        pause = false;
        Time.timeScale = 1;
    }

    void Update() {
        if (!pause) {
            // Movimiento hacia la izquierda del jugador
            if (Input.GetKey(KeyCode.A)) {
                transform.Translate(-playerSpeed, 0, 0);
                // Evita que el jugador se salga de la pantalla
                if (transform.position.x < -screenLimit) {
                    transform.position = new Vector3(-screenLimit, transform.position.y, transform.position.z);
                }
            }
            // Movimiento hacia la derecha del jugador
            if (Input.GetKey(KeyCode.D)) {
                transform.Translate(playerSpeed, 0, 0);
                // Evita que el jugador se salga de la pantalla
                if (transform.position.x > screenLimit) {
                    transform.position = new Vector3(screenLimit, transform.position.y, transform.position.z);
                }
            }
        }
        if (!PlayerHealth.final) {
            if ((Input.GetKeyDown(KeyCode.P)) || (Input.GetKeyDown(KeyCode.Escape))) {
                pause = !pause;
                pausePanel.SetActive(pause);
                if (pause) {
                    Time.timeScale = 0;
                } else {
                    Time.timeScale = 1;
                }
            }
        }
    }

    private void OnDestroy() {
        int uno = PlayerPrefs.GetInt("best1", 0);
        int dos = PlayerPrefs.GetInt("best2", 0);
        if (EnemyHealth.points > dos) {
            dos = EnemyHealth.points;
            if (EnemyHealth.points > uno) {
                dos = uno;
                uno = EnemyHealth.points;
            }
        }
        PlayerPrefs.SetInt("best1", uno);
        PlayerPrefs.SetInt("best2", dos);
    }
}